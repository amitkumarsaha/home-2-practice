<?php

include_once ("../../../../../vendor/autoload.php");
use Person\University\Department\Id\Students\Students;
$obj=new Students;
$alldata=$obj->index();
?>
<html>
    <head>
        <title>List of students</title>
    </head>
    <body>
        <table border="solid">
            <tr>
                <td>Student Id</td>
                <td>Student Name</td>
                <td>Action</td>
            </tr>
            <?php
            $count =1;
            foreach ($alldata as $value){
            ?>
            <tr>
                <td><?php echo $count++; ?></td>
                <td><?php echo $value['title']; ?></td>
                <td><a href="show.php?id=<?php echo $value['id']; ?>">Views</a>
                    <a href="delete.php?id=<?php echo $value['id']; ?>">Delete</a>
                </td>
            </tr>
            <?php } ?>
        </table>
    </body>
</html>