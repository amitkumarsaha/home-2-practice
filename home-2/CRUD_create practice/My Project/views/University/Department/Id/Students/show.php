<?php

include_once ("../../../../../vendor/autoload.php");
use Person\University\Department\Id\Students\Students;
$obj=new Students;
$obj->setData($_GET);

$value=$obj->show();
?>

<html>
<head>
    <title>List of students</title>
</head>
<body>
<a href="index.php">Back to list</a>
<table border="solid">
    <tr>
        <td>Student Id</td>
        <td>Student Name</td>
        <td>Action</td>
    </tr>

        <tr>
            <td><?php echo $value['id']; ?></td>
            <td><?php echo $value['title']; ?></td>
            <td><a href="show.php?id=<?php echo $value['id']; ?>">Views</a></td>
        </tr>

</table>
</body>
</html>
